import 'dart:async';
import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';

class PushNotificationsProvider {

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  final _messagesStreamController = StreamController<String>.broadcast();
  Stream<String> get messages => _messagesStreamController.stream;

  initNotification() {
    _firebaseMessaging.requestNotificationPermissions();

    _firebaseMessaging.getToken().then( (token) {
      print('=============');
      print(token);
      print('=============');
    });
    // cgk0ILsXBWU:APA91bGekYLbkPkSR3Pl0vHFn8H26oAdMzHnsN-Nqq8l_zsRTqabhnOAFQRrMjFpECa88vA2sBKwOkwe5evOZRRSASDQBwgqhtZhQxMTWMN0IA3QeWRdrRvyFm5sdkuV3fUUUO4X_rRr

    _firebaseMessaging.configure(
      onMessage: (info) async {
        print('====== On Message =======');
        print(info);

        String arg = 'no-data';
        if ( Platform.isAndroid ) {
          arg = info['data']['comida'] ?? 'no-data';
        }

        _messagesStreamController.sink.add(arg);
      },
      onLaunch: (info) async {
        print('====== On launch =======');
        print(info);

        String arg = 'no-data';
        if ( Platform.isAndroid ) {
          arg = info['data']['comida'] ?? 'no-data';
        }

        _messagesStreamController.sink.add(arg);
      },
      onResume: (info) async {
        print('====== On Resume =======');
        print(info);
        
        String arg = 'no-data';
        if ( Platform.isAndroid ) {
          arg = info['data']['comida'] ?? 'no-data';
        }

        _messagesStreamController.sink.add(arg);
      },
    );
  
  }

  dispose() {
    _messagesStreamController?.close();
  }

}